package org.coursera.impatientclient.model;

import java.io.Serializable;
import java.util.Date;


public class Patient implements Serializable {


    private long id;

    private User user;

    private String firstName;

    private String lastName;

    private Date dateOfBirth;

    private String medicalRecordNumber;

    private Date appointmentTime;


    public static final String PATIENT_ID = "PATIENT_ID";
    public static final String PATIENT_FIRST_NAME = "PATIENT_FIRST_NAME";
    public static final String PATIENT_LAST_NAME = "PATIENT_LAST_NAME";
    public static final String PATIENT_TIME = "PATIENT_TIME";

    public Patient() {
    }

    public Patient(long id) {
        this.id = id;
    }

    public Patient(User user, String firstName, String lastName, Date dateOfBirth, String medicalRecordNumber, Date appointmentTime) {
        this.user = user;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = (Date) dateOfBirth.clone();
        this.medicalRecordNumber = medicalRecordNumber;
        this.appointmentTime = (Date) appointmentTime.clone();
    }

    public Patient(Long id, String firstName, String lastName, Date appointmentTime) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.appointmentTime = (Date) appointmentTime.clone();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }

    public Date getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(Date appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getName() {
        return firstName + " " + lastName;
    }
}
