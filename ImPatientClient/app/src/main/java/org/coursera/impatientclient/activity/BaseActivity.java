package org.coursera.impatientclient.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import org.coursera.impatientclient.ImPatientApplication;
import org.coursera.impatientclient.R;
import org.coursera.impatientclient.model.User;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Sergey.Bychkov on 29.10.2015.
 */
public class BaseActivity extends ActionBarActivity {
    /**
     * Initialize the contents of the Activity's standard options menu.  You
     * should place your menu items in to <var>menu</var>.
     * <p/>
     * <p>This is only called once, the first time the options menu is
     * displayed.  To update the menu every time it is displayed, see
     * {@link #onPrepareOptionsMenu}.
     * <p/>
     * <p>The default implementation populates the menu with standard system
     * menu items.  These are placed in the {@link Menu#CATEGORY_SYSTEM} group so that
     * they will be correctly ordered with application-defined menu items.
     * Deriving classes should always call through to the base implementation.
     * <p/>
     * <p>You can safely hold on to <var>menu</var> (and any items created
     * from it), making modifications to it as desired, until the next
     * time onCreateOptionsMenu() is called.
     * <p/>
     * <p>When you add items to the menu, you can implement the Activity's
     * {@link #onOptionsItemSelected} method to handle them there.
     *
     * @param menu The options menu in which you place your items.
     * @return You must return true for the menu to be displayed;
     * if you return false it will not be shown.
     * @see #onPrepareOptionsMenu
     * @see #onOptionsItemSelected
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.global, menu);
        return true;
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     * <p/>
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;

            case R.id.action_logoff:
                getUser().setLoggedIn(false);
                getUser().setPassword("");
                getUser().setToken(null);
                saveUserData();
                finish();
                return true;

            case R.id.action_settings:
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }



    protected void saveUserData() {
        SharedPreferences pref = getSharedPreferences("USER", MODE_PRIVATE);
        SharedPreferences.Editor ed = pref.edit();
        ed.putLong(User.USER_ID, getUser().getId());
        ed.putString(User.USER_LOGIN, getUser().getLogin());
        ed.putString(User.USER_PASS, getUser().getPassword());
        ed.putInt(User.USER_TYPE, User.Type.toInt(getUser().getType()));
        ed.commit();
    }

    protected void loadUserData() {
        SharedPreferences pref = getSharedPreferences("USER", MODE_PRIVATE);
        if (getUser() == null) {
            setUser(new User(pref.getLong(User.USER_ID, 0L),
                    pref.getString(User.USER_LOGIN, ""),
                    pref.getString(User.USER_PASS, ""),
                    User.Type.fromInt(pref.getInt(User.USER_TYPE, 1))));
        } else {
            getUser().setId(pref.getLong(User.USER_ID, 0L));
            getUser().setLogin(pref.getString(User.USER_LOGIN, ""));
            getUser().setPassword(pref.getString(User.USER_PASS, ""));
            getUser().setType(User.Type.fromInt(pref.getInt(User.USER_TYPE, 1)));
        }

    }

    protected User getUser() {
        return ((ImPatientApplication) this.getApplication()).getUser();
    }

    protected void setUser(User user) {
        ((ImPatientApplication) this.getApplication()).setUser(user);
    }
}
