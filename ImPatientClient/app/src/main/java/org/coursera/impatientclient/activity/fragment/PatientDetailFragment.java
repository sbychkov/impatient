package org.coursera.impatientclient.activity.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.coursera.impatientclient.R;
import org.coursera.impatientclient.model.Patient;

import java.util.Date;


public class PatientDetailFragment extends DialogFragment {
    private static final String PATIENT = "patient";
    private Patient mPatient;
    private Button bEdit;
    private Button bClose;
    private EditText edFirstName;
    private EditText edLastName;
    private EditText edMedicalRecord;
    private EditText edDateOfBirth;
    private EditText edAppointmentTime;
    private boolean edMode = false;

    public static PatientDetailFragment newInstance(Patient patient) {
        PatientDetailFragment fragment = new PatientDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(PATIENT, patient);
        fragment.setArguments(args);
        return fragment;
    }

    public PatientDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPatient = (Patient) getArguments().getSerializable(PATIENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_patient_detail, container, false);

        edFirstName = (EditText) view.findViewById(R.id.edFirstName);
        edLastName = (EditText) view.findViewById(R.id.edLastName);
        edDateOfBirth = (EditText) view.findViewById(R.id.edDateOfBirth);
        edMedicalRecord = (EditText) view.findViewById(R.id.edMedicalRecordNumber);
        edAppointmentTime = (EditText) view.findViewById(R.id.edAppointmentTime);
        if (mPatient != null) {
            edFirstName.setText(mPatient.getFirstName());
            edLastName.setText(mPatient.getLastName());
            edDateOfBirth.setText(mPatient.getDateOfBirth().toString());
            edMedicalRecord.setText(mPatient.getMedicalRecordNumber());
            edAppointmentTime.setText(mPatient.getAppointmentTime().toString());
        }

        bClose = (Button) view.findViewById(R.id.bClose);
        bClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edMode) {
                    mPatient.setFirstName(edFirstName.getText().toString());
                    mPatient.setLastName(edLastName.getText().toString());
                    mPatient.setDateOfBirth(new Date(edDateOfBirth.getText().toString()));
                    mPatient.setMedicalRecordNumber(edMedicalRecord.getText().toString());
                    mPatient.setAppointmentTime(new Date(edAppointmentTime.getText().toString()));

                }
                PatientDetailFragment.this.dismiss();
            }
        });
        bEdit = (Button) view.findViewById(R.id.bEdit);
        bEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edFirstName.setEnabled(true);
                edLastName.setEnabled(true);
                edDateOfBirth.setEnabled(true);
                edMedicalRecord.setEnabled(true);
                edAppointmentTime.setEnabled(true);
            }
        });

        edFirstName.setEnabled(false);
        edLastName.setEnabled(false);
        edDateOfBirth.setEnabled(false);
        edMedicalRecord.setEnabled(false);
        edAppointmentTime.setEnabled(false);
        
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


}
