package org.coursera.impatientclient.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.coursera.impatientclient.R;

public class LoginActivity extends BaseActivity {


    private EditText edLogin;
    private EditText edPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadUserData();

        edLogin = (EditText) findViewById(R.id.edLogin);
        edLogin.setText(getUser().getLogin());
        edPassword = (EditText) findViewById(R.id.edPassword);
        edPassword.setText(getUser().getPassword());
        Button b = (Button) findViewById(R.id.bLogin);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUser().setLogin(edLogin.getText().toString());
                getUser().setPassword(edPassword.getText().toString());
                saveUserData();
                LoginActivity.this.finish();
            }
        });
    }

}
