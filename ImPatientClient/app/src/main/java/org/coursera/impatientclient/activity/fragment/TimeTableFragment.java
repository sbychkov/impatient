package org.coursera.impatientclient.activity.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import org.coursera.impatientclient.R;
import org.coursera.impatientclient.activity.ReceiverInterface;
import org.coursera.impatientclient.activity.adapter.TimeTableAdapter;
import org.coursera.impatientclient.model.Patient;
import org.coursera.impatientclient.rest.SyncResultReceiver;
import org.coursera.impatientclient.rest.SyncService;

import java.util.Collection;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimeTableFragment extends Fragment implements ReceiverInterface {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String RECEIVER = "receiver";
    SyncResultReceiver mReceiver;
    TimeTableAdapter timeTableAdapter;
    public TimeTableFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        SyncService.startActionUpdate(getActivity(), mReceiver);


        View view = inflater.inflate(R.layout.fragment_time_table, container, false);
        GridView grid = (GridView) view.findViewById(R.id.gridView);
        grid.setAdapter(timeTableAdapter);
        grid.setNumColumns(60);
        grid.setVerticalSpacing(1);
        grid.setHorizontalSpacing(1);

        grid.setMinimumHeight(10);
        return view;
    }

    public static TimeTableFragment newInstance(int sectionNumber, SyncResultReceiver receiver){
        TimeTableFragment fragment = new TimeTableFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putParcelable(RECEIVER, receiver);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        timeTableAdapter = new TimeTableAdapter();
        if (getArguments() != null) {
            mReceiver = (SyncResultReceiver) getArguments().getParcelable(RECEIVER);
        }
    }

    @Override
    public void receiveError(Bundle resultData) {

    }

    @Override
    public void receiveFinished(Bundle resultData) {
        switch (resultData.getString(SyncService.ACTION)) {
            case SyncService.ACTION_UPDATE:
               timeTableAdapter.setData((Collection<Patient>)
                       resultData.getSerializable(SyncService.DATA));
                break;
        }
    }
}
