package org.coursera.impatientclient;

import android.app.Application;

import org.coursera.impatientclient.model.User;

/**
 * Created by Sergey.Bychkov on 23.10.2015.
 */
public class ImPatientApplication extends Application {
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
