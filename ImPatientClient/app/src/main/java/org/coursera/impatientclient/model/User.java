package org.coursera.impatientclient.model;

import java.io.Serializable;


public class User implements Serializable {
    public enum Type {
        Admin, Patient;

        public static Type fromInt(int i) {
            Type result = Admin;
            if (i == 1) {
                result = Patient;
            }
            return result;
        }

        public static int toInt(Type t) {
            int result = 0;
            if (t.equals(Patient)) {
                result = 1;
            }
            return result;
        }
    }



    public static final String USER_ID = "USER_ID";
    public static final String USER_LOGIN = "USER_LOGIN";
    public static final String USER_PASS = "USER_PASSWORD";
    public static final String USER_TYPE = "USER_TYPE";

    private Long id;
    private String login;
    private String password;
    private boolean loggedIn;

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    private String token;


    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    private Type type;

    public User() {
        type=Type.Patient;
    }

    public User(Long id) {
        this();
        this.id = id;

    }

    public User(String login, String password, Type type) {
        this.login = login;
        this.password = password;
        this.type = type;
    }

    public User(Long id, String login, String password, Type type) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
