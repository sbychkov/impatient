package org.coursera.impatientclient.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.coursera.impatientclient.R;
import org.coursera.impatientclient.Utils;
import org.coursera.impatientclient.model.Patient;
import org.coursera.impatientclient.rest.SyncResultReceiver;
import org.coursera.impatientclient.rest.SyncService;

import java.util.Date;

public class PatientActivity extends ReceiverActivity implements SyncResultReceiver.Receiver {


    private Patient patient;
    private Long time;
    private TextView tPatientName;
    private TextView tPatientTime;
    private Button bCheckIn;
    private Button bPostpone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        patient = (Patient) getIntent().getSerializableExtra(Patient.PATIENT_ID);
        setContentView(R.layout.activity_patient);


        bCheckIn = (Button) findViewById(R.id.bCheckIn);
        bCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SyncService.startActionCheckIn(PatientActivity.this, patient.getId(), mReceiver);
            }
        });


        tPatientName = (TextView) findViewById(R.id.textPatientName);
        tPatientName.setText(patient.getName());

        tPatientTime = (TextView) findViewById(R.id.textTime);
        tPatientTime.setText(Utils.timeLabel(patient.getAppointmentTime().getTime()));


        bPostpone = (Button) findViewById(R.id.bPostpone);
        bPostpone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SyncService.startActionPostpone(PatientActivity.this, patient.getId(), mReceiver);
            }
        });
        bCheckIn.setEnabled(true);
        bPostpone.setEnabled(false);
    }

    @Override
    public void receiveError(Bundle bundle) {

    }

    @Override
    public void receiveFinished(Bundle resultData) {
        Date date = (Date) resultData.getSerializable(SyncService.DATA);
        time = (date != null) ? date.getTime() : 0L;
        tPatientTime.setText(Utils.timeLabel(time));
        patient.setAppointmentTime(date);
        bCheckIn.setEnabled(false);
        bPostpone.setEnabled(true);
    }

}
