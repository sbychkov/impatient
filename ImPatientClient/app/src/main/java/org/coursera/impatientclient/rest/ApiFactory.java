package org.coursera.impatientclient.rest;

import android.support.annotation.NonNull;
import android.util.Base64;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import retrofit.JacksonConverterFactory;
import retrofit.Retrofit;

public class ApiFactory {

    private static final int CONNECT_TIMEOUT = 15;
    private static final int WRITE_TIMEOUT = 60;
    private static final int TIMEOUT = 60;

    private static final OkHttpClient OK_CLIENT = new OkHttpClient();
    private static final Interceptor requestInterceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request.Builder request = chain.request().newBuilder();
            request.addHeader("User-Agent", "ImPatientClient");
            request.addHeader("Accept", "application/json");
            return chain.proceed(request.build());
        }
    };

    private static Interceptor basicAuthInterceptor(String username, String password) {
        String basic = null;
        if (username != null && password != null) {
            // concatenate username and password with colon for authentication
            String credentials = username + ":" + password;
            // create Base64 encodet string
            basic = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        }

        final String finalBasic = basic;
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder request = chain.request().newBuilder();
                request.header("Authorization", finalBasic);
                request.header("Accept", "application/json");
                request.header("User-Agent", "ImPatientClient");
                return chain.proceed(request.build());
            }
        };
    }

    static {
        OK_CLIENT.setConnectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS);
        OK_CLIENT.setWriteTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS);
        OK_CLIENT.setReadTimeout(TIMEOUT, TimeUnit.SECONDS);
        OK_CLIENT.interceptors().add(requestInterceptor);
    }

    @NonNull
    public static RestApi getRestApi() {
        return getRetrofit(RestApi.serverURL).create(RestApi.class);
    }

    @NonNull
    public static RestApi getRestApi(String serverUrl) {
        return getRetrofit(serverUrl).create(RestApi.class);
    }


    @NonNull
    public static RestApi getRestApi(String serverUrl, String user, String pass) {
        Retrofit retrofit = getRetrofit(serverUrl);
        retrofit.client().interceptors().clear();
        retrofit.client().interceptors().add(basicAuthInterceptor(user,pass));
        return retrofit.create(RestApi.class);
    }

    @NonNull
    private static Retrofit getRetrofit(String serverUrl) {
        return new Retrofit.Builder()
                .baseUrl(serverUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(OK_CLIENT)
                .build();
    }


}