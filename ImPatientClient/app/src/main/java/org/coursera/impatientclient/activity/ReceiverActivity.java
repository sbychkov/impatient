package org.coursera.impatientclient.activity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import org.coursera.impatientclient.rest.SyncResultReceiver;
import org.coursera.impatientclient.rest.SyncService;

/**
 * Created by Sergey.Bychkov on 29.10.2015.
 */
public abstract class ReceiverActivity extends BaseActivity implements SyncResultReceiver.Receiver,ReceiverInterface {
    protected SyncResultReceiver mReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mReceiver = new SyncResultReceiver(new Handler());
        mReceiver.setReceiver(this);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case SyncService.STATUS_ERROR:
                String errMessage = resultData.getString(SyncService.ERROR);
                //Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show();
                Log.e(getClass().getSimpleName(),errMessage);
                receiveError(resultData);
                break;
            case SyncService.STATUS_FINISHED:
                Log.d(getClass().getSimpleName(),resultData.getString(SyncService.ACTION));
                receiveFinished(resultData);
                break;
        }
    }

    public abstract  void receiveError(Bundle resultData);

    public abstract  void receiveFinished(Bundle resultData);
}
