package org.coursera.impatientclient;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Sergey.Bychkov on 03.11.2015.
 */
public class Utils {

    public static int getHour(Date date) {
        Calendar temp = Calendar.getInstance();
        temp.setTime(date);
        return temp.get(Calendar.HOUR_OF_DAY);
    }

    public static int getMinute(Date date) {
        Calendar temp = Calendar.getInstance();
        temp.setTime(date);
        return temp.get(Calendar.MINUTE);
    }

    public static  Date updateHoursAndMinutes(Date date, int h, int m) {
        Calendar temp = Calendar.getInstance();
        temp.setTime(date);
        temp.set(Calendar.HOUR_OF_DAY, h);
        temp.set(Calendar.MINUTE, m);
        return temp.getTime();
    }

    public static String timeLabel(Long t) {
        Date d = new Date(t);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(d);
    }


}
