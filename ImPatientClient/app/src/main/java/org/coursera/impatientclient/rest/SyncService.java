package org.coursera.impatientclient.rest;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.squareup.okhttp.ResponseBody;

import org.coursera.impatientclient.model.Patient;
import org.coursera.impatientclient.model.User;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SyncService extends IntentService {

    private static final String LOG_TAG = SyncService.class.getSimpleName();


    public static final String ACTION_UPDATE = "org.coursera.impatientclient.action.UPDATE";
    public static final String ACTION_CHECKIN = "org.coursera.impatientclient.action.CHECKIN";
    public static final String ACTION_REMOVE = "org.coursera.impatientclient.action.REMOVE";
    public static final String ACTION_MOVE = "org.coursera.impatientclient.action.MOVE";
    public static final String ACTION_POSTPONE = "org.coursera.impatientclient.action.POSTPONE";
    public static final String ACTION_GETTIME = "org.coursera.impatientclient.action.GETTIME";
    public static final String ACTION_LOGIN = "org.coursera.impatientclient.action.LOGIN";
    public static final String ACTION_GETPATIENT = "org.coursera.impatientclient.action.GETPATIENT";
    public static final String ACTION_GETUSER = "org.coursera.impatientclient.action.GETUSER";


    public static final String PATIENT_ID = "org.coursera.impatientclient.extra.PATIENT_ID";
    public static final String USER_ID = "org.coursera.impatientclient.extra.USER_ID";
    public static final String USER_LOGIN = "org.coursera.impatientclient.extra.USER_LOGIN";
    public static final String USER_PASS = "org.coursera.impatientclient.extra.USER_PASS";
    public static final String TIME = "org.coursera.impatientclient.extra.TIME";
    public static final String RECEIVER = "org.coursera.impatientclient.RECEIVER";
    public static final String DATA = "org.coursera.impatientclient.extra.DATA";
    public static final String ERROR = "org.coursera.impatientclient.ERROR";
    public static final String ERROR_CODE = "org.coursera.impatientclient.ERROR_CODE";
    public static final String ACTION = "org.coursera.impatientclient.extra.ACTION";

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    private RestApi restApi;

    /**
     * Starts this service to perform action UPDATE with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionUpdate(Context context, SyncResultReceiver rec) {
        Intent intent = new Intent(context, SyncService.class);
        intent.setAction(ACTION_UPDATE);
        intent.putExtra(RECEIVER, rec);
        context.startService(intent);
    }

    public static void startActionLogon(Context context, String login,
                                        String pass, SyncResultReceiver rec) {
        Intent i = new Intent(SyncService.ACTION_LOGIN, null, context, SyncService.class);
        i.putExtra(SyncService.USER_LOGIN, login);
        i.putExtra(SyncService.USER_PASS, pass);
        i.putExtra(SyncService.RECEIVER, rec);
        context.startService(i);
    }

    /**
     * Starts this service to perform action GETTIME with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionGetTime(Context context, Long param1, Long param2) {
        Intent intent = new Intent(context, SyncService.class);
        intent.setAction(ACTION_GETTIME);
        intent.putExtra(PATIENT_ID, param1);
        intent.putExtra(TIME, param2);
        context.startService(intent);
    }

    public static void startActionGetPatient(Context context, Long id, SyncResultReceiver rec) {
        Intent i = new Intent(SyncService.ACTION_GETPATIENT, null, context, SyncService.class);
        i.putExtra(SyncService.USER_ID, id);
        i.putExtra(SyncService.RECEIVER, rec);
        context.startService(i);
    }

    public static void startActionPostpone(Context context, Long id, SyncResultReceiver rec) {
        Intent i = new Intent(SyncService.ACTION_POSTPONE, null, context, SyncService.class);
        i.putExtra(SyncService.PATIENT_ID, id);
        i.putExtra(SyncService.RECEIVER, rec);
        context.startService(i);
    }

    public static void startActionRemove(Context context, Long id, SyncResultReceiver rec) {
        Intent i = new Intent(SyncService.ACTION_REMOVE, null, context, SyncService.class);
        i.putExtra(SyncService.PATIENT_ID, id);
        i.putExtra(SyncService.RECEIVER, rec);
        context.startService(i);
    }

    public static void startActionCheckIn(Context context, Long id, SyncResultReceiver rec) {
        Intent i = new Intent(SyncService.ACTION_CHECKIN, null, context, SyncService.class);
        i.putExtra(SyncService.PATIENT_ID, id);
        i.putExtra(SyncService.RECEIVER, rec);
        context.startService(i);
    }

    public static void startActionMove(Context context, Long id, Long time,SyncResultReceiver rec) {
        Intent i = new Intent(SyncService.ACTION_MOVE, null, context, SyncService.class);
        i.putExtra(SyncService.PATIENT_ID, id);
        i.putExtra(SyncService.TIME, time);
        i.putExtra(SyncService.RECEIVER, rec);
        context.startService(i);
    }

    public SyncService() {
        super("SyncService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        restApi = ApiFactory.getRestApi();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            ResultReceiver rec = intent.getParcelableExtra(RECEIVER);
            Log.d(LOG_TAG, action);
            switch (action) {
                case ACTION_CHECKIN:
                    handleActionCheckIn(intent.getLongExtra(PATIENT_ID, 0L), rec);
                    break;
                case ACTION_UPDATE:
                    handleActionUpdate(rec);
                    break;
                case ACTION_POSTPONE:
                    handleActionPostpone(intent.getLongExtra(PATIENT_ID, 0L), rec);
                    break;
                case ACTION_MOVE:
                    handleActionMove(intent.getLongExtra(PATIENT_ID, 0L),
                            intent.getLongExtra(TIME, 0L), rec);
                    break;
                case ACTION_REMOVE:
                    handleActionRemove(intent.getLongExtra(PATIENT_ID, 0L), rec);
                    break;
                case ACTION_GETTIME:
                    handleActionGetTime(intent.getLongExtra(PATIENT_ID, 0L), rec);
                    break;
                case ACTION_LOGIN:
                    handleActionLogin(intent.getStringExtra(USER_LOGIN),
                            intent.getStringExtra(USER_PASS), rec);
                    break;
                case ACTION_GETPATIENT:
                    handleActionGetPatientByUserId(intent.getLongExtra(USER_ID, 0L), rec);
                    break;
                case ACTION_GETUSER:
                    handleActionGetUser(intent.getLongExtra(USER_ID,0L),rec);
                    break;
            }

        }
    }

    /**
     * Handle actions
     *
     * @param rec
     */

    private void handleActionUpdate(ResultReceiver rec) {
        final Call<List<Patient>> restCall = restApi.view();
        restCall.enqueue(new MyCallback<List<Patient>>(ACTION_UPDATE, rec));

    }

    private void handleActionGetTime(Long id, ResultReceiver rec) {
        final Call<Date> restCall = restApi.getTime(id);
        restCall.enqueue(new MyCallback<Date>(ACTION_GETTIME, rec));
    }

    private void handleActionCheckIn(Long id, ResultReceiver rec) {
        final Call<Date> restCall = restApi.checkIn(id);
        restCall.enqueue(new MyCallback<Date>(ACTION_CHECKIN, rec));
    }

    private void handleActionMove(Long id, Long time, ResultReceiver rec) {
        final Call<Date> restCall = restApi.move(id, time);
        restCall.enqueue(new MyCallback<Date>(ACTION_MOVE, rec));

    }

    private void handleActionRemove(Long id, ResultReceiver rec) {
        final Call<Patient> restCall = restApi.remove(id);
        restCall.enqueue(new MyCallback<Patient>(ACTION_REMOVE, rec));

    }

    private void handleActionPostpone(Long id, ResultReceiver rec) {
        Call<Date> restCall = restApi.postpone(id);
        restCall.enqueue(new MyCallback<Date>(ACTION_POSTPONE, rec));
    }

    private void handleActionGetPatientByUserId(Long id, ResultReceiver rec) {
        final Call<Patient> restCall = restApi.getPatientByUserId(id);
        restCall.enqueue(new MyCallback<Patient>(ACTION_GETPATIENT, rec));
    }

    private void handleActionLogin(String login, String pass, ResultReceiver rec) {
        restApi = ApiFactory.getRestApi(RestApi.serverURL,login,pass);
        final Call<User> restCall = restApi.logon(login, pass);
        restCall.enqueue(new MyCallback<User>(ACTION_LOGIN, rec));
    }

    private void handleActionGetUser(Long userId, ResultReceiver rec) {
        final Call<User> restCall = restApi.getUserById(userId);
        restCall.enqueue(new MyCallback<User>(ACTION_GETUSER, rec));
    }


    /*
    *   Custom callback class implements retrofit Callback<T>
    *
    *
    * */

    private static class MyCallback<T> implements Callback<T> {

        private final String action;
        private final ResultReceiver rec;

        public MyCallback(String action, ResultReceiver rec) {
            this.action = action;
            this.rec = rec;
        }

        /**
         * Successful HTTP response.
         *
         * @param response
         */
        @Override
        public void onResponse(Response<T> response, Retrofit retrofit) {
            if (response.isSuccess()) {
                T data = response.body();
                if (rec != null) {
                    Bundle b = new Bundle();
                    b.putSerializable(DATA, (Serializable) data);
                    b.putString(ACTION, action);
                    rec.send(STATUS_FINISHED, b);
                }
                Log.d(LOG_TAG, "Success: " + action);
            } else {
                int statusCode = response.code();

                ResponseBody errorBody = response.errorBody();

                if (rec != null) {
                    Bundle b = new Bundle();
                    b.putString(ACTION, action);
                    b.putInt(ERROR_CODE, statusCode);
                    b.putString(ERROR, "Response code: " + statusCode);
                    rec.send(STATUS_ERROR, b);
                }

                Log.w(LOG_TAG, action + "\tresponse error code\t" + statusCode);
            }
        }

        /**
         * Invoked when a network or unexpected exception occurred during the HTTP request.
         *
         * @param t
         */
        @Override
        public void onFailure(Throwable t) {
            if (rec != null) {
                Bundle b = new Bundle();
                b.putString(ACTION, action);
                b.putString(ERROR, "Connection to server failed on " + action);
                rec.send(STATUS_ERROR, b);
            }
            Log.e(LOG_TAG, "Connection to server failed on " + action);
        }
    }


}
