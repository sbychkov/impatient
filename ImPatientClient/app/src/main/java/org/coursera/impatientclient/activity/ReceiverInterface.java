package org.coursera.impatientclient.activity;

import android.os.Bundle;

/**
 * Created by Sergey.Bychkov on 02.11.2015.
 */
public interface ReceiverInterface {
    void receiveError(Bundle resultData);

    void receiveFinished(Bundle resultData);
}
