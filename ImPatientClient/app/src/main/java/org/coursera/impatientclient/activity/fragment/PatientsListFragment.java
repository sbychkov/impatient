package org.coursera.impatientclient.activity.fragment;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TimePicker;

import org.coursera.impatientclient.R;
import org.coursera.impatientclient.Utils;
import org.coursera.impatientclient.activity.ReceiverInterface;
import org.coursera.impatientclient.activity.adapter.PatientsAdapter;
import org.coursera.impatientclient.model.Patient;
import org.coursera.impatientclient.rest.SyncResultReceiver;
import org.coursera.impatientclient.rest.SyncService;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;


public class PatientsListFragment extends android.support.v4.app.Fragment
        implements SwipeRefreshLayout.OnRefreshListener, ReceiverInterface {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String RECEIVER = "receiver";
    private PatientsAdapter patientsAdapter;
    private SyncResultReceiver mReceiver;
    private Patient selectedPatient;
    private ListView list;

    public PatientsListFragment() {
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mReceiver = (SyncResultReceiver) getArguments().getParcelable(RECEIVER);
        }
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PatientsListFragment newInstance(int sectionNumber,SyncResultReceiver receiver) {
        PatientsListFragment fragment = new PatientsListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putParcelable(RECEIVER, receiver);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_admin, container, false);

        list = (ListView) rootView.findViewById(R.id.patientListView);
        patientsAdapter = new PatientsAdapter(this.getActivity());

        list.setAdapter(patientsAdapter);
        list.setSelector(R.color.button_material_light);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedPatient = patientsAdapter.getItem(position);
            }
        });

        SyncService.startActionUpdate(getActivity(), mReceiver);

        Button bRemove = (Button) rootView.findViewById(R.id.bRemove);
        bRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPatient != null && selectedPatient.getId() > 0) {
                    SyncService.startActionRemove(getActivity(), selectedPatient.getId(),
                            mReceiver);
                }
            }
        });
        Button bDetail = (Button) rootView.findViewById(R.id.bDetails);
        bDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPatient != null && selectedPatient.getId() > 0) {
                    SyncService.startActionGetPatient(getActivity(), selectedPatient.getId()
                            , mReceiver);
                }
            }
        });
        Button bAdjustTime = (Button) rootView.findViewById(R.id.bAdjustTime);
        bAdjustTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedPatient != null) {
                    TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                            new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            selectedPatient.setAppointmentTime(Utils.updateHoursAndMinutes(
                                    selectedPatient.getAppointmentTime(), hourOfDay, minute));
                            SyncService.startActionMove(getActivity(), selectedPatient.getId(),
                                    selectedPatient.getAppointmentTime().getTime(), mReceiver);
                        }
                    }, Utils.getHour(selectedPatient.getAppointmentTime()),
                            Utils.getMinute(selectedPatient.getAppointmentTime()), true);
                    timePickerDialog.show();
                }
            }
        });
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

    }




    @Override
    public void onRefresh() {
        SyncService.startActionUpdate(getActivity(), mReceiver);
    }


    @Override
    public void receiveError(Bundle resultData) {

    }

    @Override
    public void receiveFinished(Bundle resultData) {
        switch (resultData.getString(SyncService.ACTION)) {
            case SyncService.ACTION_UPDATE:
                patientsAdapter.setData((Collection<Patient>)
                        resultData.getSerializable(SyncService.DATA));
                break;
            case SyncService.ACTION_MOVE:
                SyncService.startActionUpdate(getActivity(), mReceiver);
                break;
            case SyncService.ACTION_REMOVE:
                SyncService.startActionUpdate(getActivity(), mReceiver);
                break;
            case SyncService.ACTION_GETPATIENT:
                PatientDetailFragment pdf = PatientDetailFragment.newInstance(selectedPatient);
                pdf.show(getActivity().getSupportFragmentManager(), "Patient Details");

                break;
        }
    }
}
