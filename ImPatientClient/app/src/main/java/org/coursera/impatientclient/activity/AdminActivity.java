package org.coursera.impatientclient.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.widget.ListView;

import org.coursera.impatientclient.R;
import org.coursera.impatientclient.activity.adapter.PatientsAdapter;
import org.coursera.impatientclient.activity.adapter.TimeTableAdapter;
import org.coursera.impatientclient.activity.fragment.NavigationDrawerFragment;
import org.coursera.impatientclient.activity.fragment.PatientsListFragment;
import org.coursera.impatientclient.activity.fragment.TimeTableFragment;
import org.coursera.impatientclient.model.Patient;
import org.coursera.impatientclient.rest.SyncResultReceiver;

import java.util.List;

public class AdminActivity extends ReceiverActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private FragmentManager fragmentManager;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void receiveError(Bundle resultData) {
        ((ReceiverInterface) getCurrentFragment()).receiveError(resultData);
    }

    @Override
    public void receiveFinished(Bundle resultData) {
        ((ReceiverInterface) getCurrentFragment()).receiveFinished(resultData);
    }

    private Fragment getCurrentFragment() {
       Fragment currentFragment= null;
        List<Fragment> fragments = fragmentManager.getFragments();
        for(Fragment f:fragments){
            if(f instanceof ReceiverInterface  && f.isVisible()){
                currentFragment=f;
            }
        }
        return currentFragment;
    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        if (fragmentManager == null) {
            fragmentManager = getSupportFragmentManager();
        }
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1, mReceiver))
                .commit();
    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = getString(R.string.title_section1);
                break;
            case 2:
                mTitle = getString(R.string.title_section2);
                break;

        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private PatientsAdapter patientsAdapter;
        private SyncResultReceiver mReceiver;
        private Patient selectedPatient;
        private ListView list;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static Fragment newInstance(int sectionNumber, SyncResultReceiver mReceiver) {
            Fragment fragment = null;
            switch (sectionNumber) {
                case 1:
                    fragment = PatientsListFragment.newInstance(sectionNumber, mReceiver);
                    break;
                case 2:
                    fragment = TimeTableFragment.newInstance(sectionNumber,mReceiver);
                    break;

            }


            return fragment;
        }

        public PlaceholderFragment() {

        }


    }

}
