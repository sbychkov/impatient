package org.coursera.impatientclient.rest;

import org.coursera.impatientclient.model.Patient;
import org.coursera.impatientclient.model.User;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Sergey.Bychkov on 25.09.2015.
 */
public interface RestApi {
    String QUEUE = "queue/";
    String QUEUE_PATIENT = "queue/{patient}/";
    String LOGIN = "logon/";
    String REGID = "regid/";
    String USER = "users/";
    String PATIENT = "patients/";
    String serverURL = "https://wildfly8-sbychkov.rhcloud.com/ImPatientServer/";
    //String serverURL = "http://10.106.2.210:8080/";


    @POST(QUEUE_PATIENT)
    Call<Date> checkIn(@Path("patient") Long id);

    @PUT(QUEUE_PATIENT)
    Call<Date> move(@Path("patient") Long id, @Query("time") Long time);

    @DELETE(QUEUE_PATIENT)
    Call<Patient> remove(@Path("patient") Long id);

    @GET(QUEUE_PATIENT)
    Call<Date> getTime(@Path("patient") Long id);

    @PUT(QUEUE_PATIENT+"postpone")
    Call<Date> postpone(@Path("patient") Long id);

    @GET(QUEUE)
    Call<List<Patient>> view();

    @POST(LOGIN)
    @FormUrlEncoded
    Call<User> logon(@Field("login") String login, @Field("password") String pass);

    @POST(REGID)
    @FormUrlEncoded
    Call<User> regId(@Field("id") Long id, @Field("regid") String regid);

    @GET(PATIENT + "search/findByUserId")
    Call<Patient> getPatientByUserId(@Query("id") Long userId);

    @GET(PATIENT+"{id}/")
    Call<Patient> getPatientById(@Query("id") Long patientId);

    @GET(USER+"{id}/")
    Call<User> getUserById(@Query("id") Long userId);


}
