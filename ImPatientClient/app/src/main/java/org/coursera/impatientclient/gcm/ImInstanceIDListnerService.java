package org.coursera.impatientclient.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

public class ImInstanceIDListnerService extends InstanceIDListenerService {

    private static final String LOG_TAG = ImInstanceIDListnerService.class.getSimpleName();


    public ImInstanceIDListnerService() {
    }

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}
