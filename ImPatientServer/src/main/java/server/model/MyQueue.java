package server.model;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.TreeSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.springframework.beans.factory.annotation.Autowired;


@Component
public class MyQueue implements IQueue {
    private final PatientComparator cmp = new PatientComparator();
    private final TreeSet<Patient> queue = new TreeSet<>(cmp);
    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock r = rwl.readLock();
    private final Lock w = rwl.writeLock();
    
    @Autowired
    private MessageService ms;

    public MyQueue() {
    }


    @Override
    public Date checkIn(Patient p) {
        w.lock();
        try {
            if (findById(p) == null) {
                if (queue.isEmpty()) {
                    p.setAppointmentTime(adjustTime(new Date()));
                } else {
                    p.setAppointmentTime(plus15(queue.last().getAppointmentTime()));
                }
                queue.add(p);
                ms.setClock(queue.first().getAppointmentTime());
            } else {
                p.setAppointmentTime(null);
            }
        } finally {
            w.unlock();
        }
       
        return p.getAppointmentTime();
    }

    @Override
    public Date move(Patient p, Date d) {
        d = adjustTime(d);
        w.lock();
        try {
            Patient patient = findById(p);
            if (patient != null) {
                queue.remove(patient);
                patient.setAppointmentTime(d);
                recursiveMove(findByTime(d));
                queue.add(patient);
                ms.setClock(queue.first().getAppointmentTime());
            }else{
                d=null;
            }
        } finally {
            w.unlock();
        }
        return d;
    }

    private void recursiveMove(Patient p) {
        if (p != null) {
            Patient higher = queue.higher(p);
            if (higher != null && cmp.compare(p, higher) == 0) {
                recursiveMove(higher);
            } else {
                queue.remove(p);
                p.setAppointmentTime(plus15(p.getAppointmentTime()));
                queue.add(p);
            }
        }
    }


    @Override
    public Date postpone(Patient p) {
        return move(p, plus5(p.getAppointmentTime()));
    }

    @Override
    public void remove(Patient p) {
        w.lock();
        try {
            queue.remove(findById(p));
            ms.setClock(queue.first().getAppointmentTime());
        } finally {
            w.unlock();
        }
    }

    @Override
    public Date get(Patient p) {
        Date result = null;
        r.lock();
        try {
            Patient temp = findById(p);
            if (temp != null) {
                result = temp.getAppointmentTime();
            }
        } finally {
            r.unlock();
        }
        return result;
    }

    private Patient findById(Patient p) {
        Patient result = null;
        for (Patient item : queue) {
            if (item.getId().equals(p.getId())) {
                result = item;
                break;
            }
        }
        return result;
    }

    private Patient findByTime(Date date) {
        Patient result = null;
        Patient patient = new Patient();
        patient.setAppointmentTime(date);
        for (Patient item : queue) {
            if (cmp.compare(item, patient) == 0) {
                result = item;
                break;
            }
        }
        return result;
    }


    @Override
    public Collection<Patient> view() {
        Collection<Patient> result = null;
        r.lock();
        try {
            result = queue;
        } finally {
            r.unlock();
        }
        return result;
    }


    private Date plus15(Date d) {
        if (d == null) {
            d = new Date(0L);
        }
        return new Date(d.getTime() + 15 * 60000);
    }

    private Date plus5(Date d) {
        if (d == null) {
            d = new Date(0L);
        }
        return new Date(d.getTime() + 5 * 60000);
    }

    private Date adjustTime(Date d) {
        Date result;
        if (d == null) {
            d = new Date(0L);
        }

        if (d.getTime() % 60000 == 0) {
            result = d;
        } else {
            result = new Date((d.getTime() / 60000 + 1) * 60000);
        }
        return result;
    }


    private class PatientComparator implements Comparator<Patient> {
        
        @Override
        public int compare(Patient o1, Patient o2) {
            int result = 0;

            Long id1 = (o1 != null) ? o1.getId() : null;
            Long id2 = (o2 != null) ? o2.getId() : null;

            if (id1 != null && id2 != null && !id1.equals(id2)) {

                Date t1 = (o1 != null) ? o1.getAppointmentTime() : null;
                Date t2 = (o2 != null) ? o2.getAppointmentTime() : null;

                if (t1 == null || t1.compareTo(plus15(t2)) < 0) {
                    result = -1;
                } else if (t2 == null || plus15(t1).compareTo(t2) > 0) {
                    result = 1;
                }
            }
            return result;
        }
    }
}


