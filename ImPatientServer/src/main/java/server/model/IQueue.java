package server.model;

import java.util.Collection;
import java.util.Date;



public interface IQueue {

    Date checkIn(Patient p);

    Date move(Patient p, Date d);

    Date postpone(Patient p);

    void remove(Patient p);

    Date get(Patient p);

    Collection<Patient> view();
}
