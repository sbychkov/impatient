package server.model;

import server.model.User;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;

@Entity
public class Patient implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private User user;

    private String firstName;

    private String lastName;

    private Date dateOfBirth;

    private String medicalRecordNumber;

    private Date appointmentTime;

    public Patient() {
    }

    public Patient(long id) {
        this.id = id;
    }

    public Patient(User user, String firstName, String lastName, Date dateOfBirth, String medicalRecordNumber, Date appointmentTime) {
        this.user = user;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = (Date) dateOfBirth.clone();
        this.medicalRecordNumber = medicalRecordNumber;
        this.appointmentTime = (Date) appointmentTime.clone();
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }

    public Date getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(Date appointmentTime) {
        if (appointmentTime != null) {
            this.appointmentTime = (Date) appointmentTime.clone();
        } else {
            this.appointmentTime = null;
        }
    }


}
