package server.ctrl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import server.repo.AdminRepository;
import server.repo.PatientRepository;
import server.repo.UserRepository;
import server.model.User;

/**
 * Created by Sergey.Bychkov on 23.10.2015.
 */
@RestController
public class LogonController {

    @Autowired
    private UserRepository users;

    private final Log log = LogFactory.getLog(getClass());
    @Autowired
    private AdminRepository admins;
    @Autowired
    private PatientRepository patients;

    @RequestMapping(value = "logon/", method = RequestMethod.POST)
    public User logon(@RequestParam("login") String login, @RequestParam("password") String password,
            HttpServletResponse response) {
        User user = null;
        if (login != null && !login.isEmpty()) {
            User byLogin = users.findByLogin(login);
            if (password != null && byLogin.getPassword().equals(password)) {
                user = byLogin;
                log.error("auth user: " + user.toString());
            }
        } else {
            log.error("unauth user: " + login + " pass: " + password);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
        if (user == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return user;
    }

    @RequestMapping(value = "regid/", method = RequestMethod.POST)
    public void setRegId(@RequestParam("id") Long userId, 
            @RequestParam("regid") String regId, HttpServletResponse response) {
        if (userId!=null){
             User user = users.getOne(userId);
             user.s
        }

    }
}
