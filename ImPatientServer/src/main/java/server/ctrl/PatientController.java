package server.ctrl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

import server.repo.PatientRepository;
import server.model.Patient;

/**
 * Created by Sergey.Bychkov on 28.10.2015.
 */
@RestController
public class PatientController  {

    @Autowired
    private PatientRepository patients;
    @RequestMapping(value = "patients/search/findByUserId" , method = RequestMethod.GET)
    public Patient getByUserId(@RequestParam("id") Long id,HttpServletResponse response){
       Patient patient = patients.findByUserId(id);
        if (patient==null){
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return patient;
    }

    @RequestMapping(value = "patients/{id}/", method=RequestMethod.GET)
    public  Patient getById(@PathVariable("id") Long id,HttpServletResponse response){
        Patient patient = patients.findOne(id);
        if (patient==null){
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return patient;
    }

}
