package server.ctrl;


import java.util.Collection;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import server.model.Admin;
import server.model.IQueue;
import server.model.Patient;
import server.model.User;
import server.repo.AdminRepository;
import server.repo.PatientRepository;
import server.repo.UserRepository;


@RestController
public class QueueController {

    @Autowired
    private IQueue queue;
    @Autowired
    private PatientRepository patients;
    @Autowired
    private UserRepository users;
    @Autowired
    private AdminRepository admins;


    @RequestMapping(value = "/queue/{id}", method = RequestMethod.POST)
    public Date checkIn(@PathVariable(value = "id") Long id, HttpServletResponse response) {
        Date date = null;
        Patient p = patients.findOne(id);
        if (p != null) {
            date = queue.checkIn(p);
            if (date==null){
                response.setStatus(HttpServletResponse.SC_CONFLICT);
            }
        }else{
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return date;
    }


    @RequestMapping(value = "/queue/{id}", method = RequestMethod.PUT)
    public Date move(@PathVariable(value = "id") Long id, @RequestParam(value = "time") Long time,
                     HttpServletResponse response) {
        Date move = null;
        Patient p = patients.findOne(id);
        if (p != null) {
            move = queue.move(p, new Date(time));
        }else{
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return move;
    }

    @RequestMapping(value = "/queue/{id}/postpone", method = RequestMethod.PUT)
    public Date postpone(@PathVariable(value = "id") Long id, HttpServletResponse response) {
        Date move = null;
        Patient p = patients.findOne(id);
        if (p != null) {
            move = queue.postpone(p);
        }else{
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return move;
    }

    @RequestMapping(value = "/queue/{id}", method = RequestMethod.DELETE)
    public Patient remove(@PathVariable(value = "id") Long id, HttpServletResponse response) {
        Patient p = patients.findOne(id);
        if (p != null) {
            queue.remove(p);
        }else{
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return p;
    }

    @RequestMapping(value = "/queue/{id}", method = RequestMethod.GET)
    public Date getTime(@PathVariable(value = "id") Long id, HttpServletResponse response) {
        Date date = null;
        Patient p = patients.findOne(id);
        if (p != null) {
            date = queue.get(p);
        }else{
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return date;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/queue/", method = RequestMethod.GET)
    public @ResponseBody Collection<Patient> view() {
        Collection<Patient> view = queue.view();
        return view;
    }


   /*

     Seed db for test data

    */

    @PostConstruct
    public void ttt() {
        User u = users.save(new User("user", "pass", User.Type.Patient));
        Patient p = patients.save(new Patient(u, "john", "doe", new Date(), "fjf", new Date()));
        checkIn(p.getId(),null);
        u = users.save(new User("user2", "pass2", User.Type.Admin));
        admins.save(new Admin(u));
        p = patients.save(new Patient(u, "jane", "doe", new Date(), "f55jf", new Date()));
        checkIn(p.getId(),null);
        u = users.save(new User("user3", "pass3", User.Type.Patient));
        patients.save(new Patient(u, "Ivan", "-", new Date(), "4f55jf", new Date()));
        u = users.save(new User("user4", "pass4", User.Type.Patient));
        patients.save(new Patient(u, "G.I.", "Jho", new Date(), "f55jf45", new Date()));
    }

}
