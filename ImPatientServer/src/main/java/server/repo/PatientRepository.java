package server.repo;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import server.model.Patient;
import server.model.User;

public interface PatientRepository extends JpaRepository<Patient, Long> {
    Patient findByUser(@Param("user")User u);
    @Query("Select p from Patient p where p.user.id = :id")
    Patient findByUserId(@Param("id")Long id);
}
