package server.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import server.model.Admin;
import server.model.User;



/**
 * Created by 1 on 25.10.2015.
 */
public interface AdminRepository extends JpaRepository<Admin,Long>{
    Admin findByUser(User user);
}
