package server.repo;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import server.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByLogin(@Param("login")String login);
}
