package server;

import junit.framework.TestCase;

import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServerApplication.class)
@WebAppConfiguration
public class ServerApplicationTests extends TestCase{

	private RestTemplate restTemplate = new TestRestTemplate();

	@Test
	public void contextLoads() {
	}
	@Test
	public void testAlps(){
		String result;

		result = restTemplate.getForObject("http://localhost:8080/",String.class,createHeaders("user","pass"));
		assertNotNull(result);
		System.out.println("\n\n\nresult:\t" + result+ "\nresult class:\t"+ result.getClass().getName());
		result = restTemplate.getForObject("http://localhost:8080/alps/", String.class);
		assertNotNull(result);
		System.out.println("\n\n\nresult:\t" + result+ "\nresult class:\t"+ result.getClass().getName());
	}



	private HttpHeaders createHeaders( final String username, final String password ){
		return new HttpHeaders(){
			{
				String auth = username + ":" + password;
				byte[] encodedAuth = Base64.encodeBase64(
						auth.getBytes(Charset.forName("US-ASCII")));
				String authHeader = "Basic " + new String( encodedAuth );
				set( "Authorization", authHeader );
			}
		};
	}

}
