package model;

import server.model.MyQueue;
import server.model.Patient;
import junit.framework.TestCase;

import org.junit.Test;

import java.util.Collection;
import java.util.Date;
import java.util.TreeSet;


/**
 * Created by 1 on 01.11.2015.
 */
public class MyQueueTest extends TestCase {

    private MyQueue queue;
    private Patient p1;
    private Patient p2;
    private Patient p3;
    private Patient p4;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        p1 = new Patient(null, "Number", "One", new Date(0L), "qwer12345", new Date(5900050L));
        p1.setId(1L);
        p2 = new Patient(null, "Number", "Two", new Date(0L), "qwer12346", new Date(6900000L));
        p2.setId(2L);
        p3 = new Patient(null, "Number", "Three", new Date(0L), "qwer12347", new Date(7800000L));
        p3.setId(3L);
        p4 = new Patient(null, "Number", "Four", new Date(0L), "qwer12348", new Date(8700000L));
        p4.setId(4L);
    }

    @Test
    public void testCheckIn() throws Exception {
        queue = new MyQueue();
        queue.checkIn(p1);
        queue.checkIn(p2);
        queue.checkIn(p3);
        queue.checkIn(p4);

        assertEquals(((TreeSet) queue.view()).first(), p1);
        assertEquals(((TreeSet) queue.view()).last(), p4);

        queue.checkIn(p1);
        queue.checkIn(p2);
        queue.checkIn(p3);
        queue.checkIn(p4);

        assertEquals(4, queue.view().size());
    }

    @Test
    public void testMove() throws Exception {
        queue = new MyQueue();
        queue.checkIn(p1);
        queue.checkIn(p2);
        queue.checkIn(p3);
        queue.checkIn(p4);

        queue.move(p3, new Date());

        assertTrue(queue.view().size() == 4);

    }

    @Test
    public void testPostpone() throws Exception {
        queue = new MyQueue();
        queue.checkIn(p1);
        queue.checkIn(p2);
        queue.checkIn(p3);
        queue.checkIn(p4);

        Long time = queue.get(new Patient(1L)).getTime();

        queue.postpone(p1);
        Long time2 = queue.get(new Patient(1L)).getTime();
        assertTrue(time + 300000 == time2);
    }

    @Test
    public void testRemove() throws Exception {
        queue = new MyQueue();
        queue.checkIn(p1);
        queue.checkIn(p2);
        queue.checkIn(p3);
        queue.checkIn(p4);

        queue.remove(new Patient(3L));
        assertTrue(queue.view().size() == 3);
        assertFalse(queue.view().contains(p3));
    }

    @Test
    public void testGet() throws Exception {
        queue = new MyQueue();
        queue.checkIn(p1);
        queue.checkIn(p2);
        queue.checkIn(p3);
        queue.checkIn(p4);

        Date result = queue.get(new Patient(2L));
        assertEquals(p2.getAppointmentTime(), result);
    }

    @Test
    public void testView() throws Exception {
        queue = new MyQueue();
        queue.checkIn(p1);
        queue.checkIn(p2);
        queue.checkIn(p3);
        queue.checkIn(p4);

        Collection<Patient> view = queue.view();
        assertTrue(view.size() == 4);

    }
}