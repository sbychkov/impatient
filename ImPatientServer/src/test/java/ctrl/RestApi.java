package ctrl;

import java.util.Collection;
import java.util.Date;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import server.model.Patient;
import server.model.User;


/**
 * Created by Sergey.Bychkov on 21.10.2015.
 */
public interface RestApi {
    String QUEUE = "queue/";
    String QUEUE_PATIENT = "queue/{patient}/";
    String LOGIN = "logon/";
    String USER = "users/";
    String PATIENT = "patients/";
    String serverURL = "http:/127.0.0.1:8080/";


    @POST(QUEUE_PATIENT)
    Call<Date> checkIn(@Path("patient") Long id);

    @PUT(QUEUE_PATIENT)
    Call<Date> move(@Path("patient") Long id, @Query("time") Long time);

    @DELETE(QUEUE_PATIENT)
    Call<Patient> remove(@Path("patient") Long id);

    @GET(QUEUE_PATIENT)
    Call<Date> getTime(@Path("patient") Long id);

    @PUT(QUEUE_PATIENT+"postpone")
    Call<Date> postpone(@Path("patient") Long id);

    @GET(QUEUE)
    Call<Collection<Patient>> view();

    @POST(LOGIN)
    @FormUrlEncoded
    Call<User> logon(@Field("login") String login, @Field("password") String pass);

    @GET(PATIENT + "search/findByUserId")
    Call<Patient> getPatientByUserId(@Query("id") Long userId);

    @GET(PATIENT+"{id}/")
    Call<Patient> getPatientById(@Query("id") Long patientId);

    @GET(USER+"{id}/")
    Call<User> getUserById(@Query("id") Long userId);


}
