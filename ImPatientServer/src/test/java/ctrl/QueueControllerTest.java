


package ctrl;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;

import junit.framework.TestCase;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import server.repo.PatientRepository;
import retrofit.Call;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;
import server.model.Patient;


/**
 * Created by Sergey.Bychkov on 20.10.2015.
 */
public class QueueControllerTest extends TestCase {

    private RestApi rest;

    @Autowired
    PatientRepository patients;


    public void setUp() throws Exception {
        super.setUp();

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.interceptors().add(basicAuthInterceptor("user", "pass"));

        rest = new Retrofit.Builder()
                .baseUrl(RestApi.serverURL)
                .client(okHttpClient)
                .addConverterFactory(JacksonConverterFactory.create())
                .build().create(RestApi.class);
    }

    private static Interceptor basicAuthInterceptor(String username, String password) {
        String basic = null;
        if (username != null && password != null) {
            String credentials = username + ":" + password;
            basic = "Basic " + Base64.encodeBase64String(credentials.getBytes());
        }

        final String finalBasic = basic;
        return new Interceptor() {
            @Override
            public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                Request.Builder request = chain.request().newBuilder();
                request.header("Authorization", finalBasic);
                request.header("Accept", "application/json");
                request.header("User-Agent", "ImPatientClient");
                return chain.proceed(request.build());
            }
        };
    }

    public void tearDown() throws Exception {

    }

    public void testCheckIn() throws Exception {
        Call<Date> call = rest.checkIn(3L);
        Response response = call.execute();
        assertNotNull(response);
        System.out.println(response);
        Object body = response.body();
        assertNotNull(body);
        System.out.println(body);
    }

    public void testMove() throws Exception {
        Call<Date> call = rest.move(1L, (new Date()).getTime() + 1500000);
        Response response = call.execute();
        assertNotNull(response);
        System.out.println(response);
        Object body = response.body();
        assertNotNull(body);
        System.out.println(body);
    }

    public void testRemove() throws Exception {
        Call<Patient> call = rest.remove(1L);
        Response response = call.execute();
        assertNotNull(response);
        System.out.println(response);
        Object body = response.body();
        assertNotNull(body);
        System.out.println(body);
    }

    public void testView() throws Exception {
        Call<Collection<Patient>> view = rest.view();
        Response response = view.execute();
        assertNotNull(response);
        System.out.println(response);
        Object body = response.body();
        assertNotNull(body);
        System.out.println(body);

    }
}
